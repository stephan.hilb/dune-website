+++
date = "2016-12-06"
title = "Dune 2.5.0rc2 Released"
+++

The second release candidate for the upcoming 2.5 release is now available.
You can [download the tarballs](/releases/2.5.0rc2), checkout the `v2.5.0rc2`
tag via Git, or get prebuilt packages from Debian unstable. Please go
and test, and report any problems that you encounter.

Included in the release candidate are the [core modules][] (dune-common,
dune-geometry, dune-grid, dune-grid-howto, dune-istl, dune-localfunctions)
and several modules from the [staging area][] (dune-functions, dune-typetree,
dune-uggrid).

Please refer to the [recent changes] page for an overview of what has
changed in the new release.  If anything is missing, please amend the
list.

There have been only minor changes since the first release candidate
and we hope to publish the final release soon.

  [core modules]: https://gitlab.dune-project.org/core/
  [staging area]: https://gitlab.dune-project.org/staging/
  [recent changes]: /dev/recent-changes
