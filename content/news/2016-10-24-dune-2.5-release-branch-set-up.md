+++
date = "2016-10-24"
title = "DUNE 2.5: release branches set up, please test"
+++

The release branches for the upcoming DUNE 2.5 release have been set
up last week (named `releases/2.5`).  Please test it!

Note that changes in `master` will not be automatically picked up by
us. If there are issues or merge requests that you would like to see
addressed in 2.5 please,

 - for issues: make sure the issue is assigned to the
   ["DUNE 2.5.0" milestone][DUNE250]
 - for merge requests: make sure the merge request is against the
   `releases/2.5` branch, assigned to the "DUNE 2.5.0" milestone and
   the CI tests pass.

For merge requests already accepted into master, GitLab provides a
convenient "Cherry-pick" button to create a new merge request against
`releases/2.5`.

Note that non-critical issues will not block the release.  Please make
sure a merge request is provided in time or it might have to wait for
2.5.1 or 2.6.0.  As a short reminder, the first release candidate is
planned for November, 7th, that is in two weeks.

  [DUNE250]: https://gitlab.dune-project.org/groups/core/milestones/dune-250?title=DUNE+2.5.0
  [changes]: https://www.dune-project.org/dev/recent-changes/
