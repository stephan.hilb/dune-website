+++
date = "2009-09-30"
title = "New Grid Implementation Available: GeometryGrid"
+++

A new meta implementation of the grid interface, the **GeometryGrid**, has been added to dune-grid. GeometryGrid wraps any other implementation of the DUNE grid interface (called the host grid) and replaces its geometry by a new piecewise linear one. To this end, the user must provide a coordinate function to calculate the corners of these geometries. Anything from an analytical function, mapping the corners of the host grid into some (possibly higher dimensional) space, and a continuous discrete function (given in the vertices) is supported. GeometryGrid is implemented using the generic geometries, and anybody interested in this very powerful feature should have a look in the code.

Have a look at the [documentation](http://www.dune-project.org/doc/doxygen/dune-grid-html/group__GeoGrid.html) for further details.
