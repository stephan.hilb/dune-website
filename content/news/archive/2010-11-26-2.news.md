+++
date = "2010-11-26T12:05:00+01:00"
title = "2010 DUNE Developer Meeting"
+++

A meeting of all DUNE developers was held on 22. and 23. of November 2010 in Münster.

The results of the DUNE User Meeting and the [user poll](/pdf/meetings/2010-10-usermeeting/discussion_dum2010.pdf) were dicussed. A major topic was the interface for finite elements, for values in the global space. Further discussions cover small changes and performance improvements in the grid interface. We also elected Carsten Gräser as a new DUNE Core Developer

Please refer to the [meating minutes](/community/meetings/2010-11-devmeeting/) for further details.
