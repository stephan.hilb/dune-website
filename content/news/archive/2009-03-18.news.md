+++
date = "2009-03-18"
title = "dune-subgrid released"
+++

We are proud to announce the first official release of the `dune-subgrid` module. This module provides the meta grid `SubGrid`, which allows to do computations on subsets of elements of Dune grids. The module is available for download from the [project homepage](http://numerik.mi.fu-berlin.de/dune-subgrid/index.php). There is also a [paper](http://www.matheon.de/preprints/5868_graeser_sander_subgrid.pdf) with the module documentation and some example applications.
