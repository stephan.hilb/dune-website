+++
date = "2006-06-25"
title = "New Domain"
+++

Dune now has its own domain.

For a more convenient access to Dune-related information we registered [dune-project.org](http://www.dune-project.org).
