+++
date = "2013-02-27"
title = "Dune 2.2.1 Released"
+++

We are proud the announce the release of the new stable version 2.2.1 of the Dune core modules. Dune 2.2.1 is a bugfix release based on the previous 2.2 release. We have made every effort to backport all bugfixes from our development branch. These include various fixes to better support newer compilers like g++-4.7 and clang and newer versions of the external software used. Dune 2.2.1 is supposed to be fully backward compatible to the 2.2 release. To get the latest bugfixes users of Dune 2.2 are highly advised to switch to the new release. You can read the [release notes]($(ROOT)/releasenotes/releasenotes-2.2.html) for all the details. Tarballs and svn access is available on our [download]($(ROOT)/download.html) page. We hope you enjoy this new release, and recommend it to your friends and family. If you encounter any problems please let us know by way of the [bugtracker](http://www.dune-project.org/flyspray/).
