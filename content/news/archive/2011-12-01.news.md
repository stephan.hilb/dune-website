+++
date = "2011-12-01"
title = "New ALUGrid Version"
+++

ALUGrid version 1.50 has been released. This version adds new features, most notably

-   support for quadrilateral grids in 2d, ALUCubeGrid&lt;2,2&gt;,
-   2d grids embedded in 3d, providing ALU{Cube,Simplex,Conform}Grid&lt;2,3&gt;,
-   support for ParMETIS (experimental),

and also some bug fixes. The new version is available from the [ALUGrid page](http://www.mathematik.uni-freiburg.de/IAM/Research/alugrid/).
