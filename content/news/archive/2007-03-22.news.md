+++
date = "2007-03-22"
title = "Dune 1.0beta3 available"
+++

Today we released the third an hopefully final beta release for the three core modules `dune-common`, `dune-grid`, `dune-istl` and the `dune-grid-howto`.

Go to the *download page*, grab the packages, test them!
