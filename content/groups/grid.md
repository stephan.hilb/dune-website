+++
group = "grid"
title = "Grid Modules"
[menu.main]
parent = "modules"
weight = 2
+++
### Grid managers

The dune-grid module already includes some grid managers. Some are DUNE internal but most require some additional packages. Look at an [overview](/doc/gridmanager-features) of the different grid manager provided and their features. In addition to those provided directly with dune-grid, there are further grid managers with different sets of features available - some of these might require the installation of some additional library. Not all the grid managers listed here are available for download but feel free to contact the maintainer to ask for a tar ball.

* [dune-prismgrid](http://www.mathematik.uni-freiburg.de/IAM/homepages/gersbach/prismgrid/) is a grid wrapper for a DUNE grid implementation, adding an additional dimension to the grid using generic prismatic elements. This grid is maintained by [Christoph Gersbacher](http://www.mathematik.uni-freiburg.de/IAM/homepages/gersbach/index.html).
