+++
group = ["grid"]
module = "dune-foamgrid"
requires = ["dune-common", "dune-geometry", "dune-grid"]
short = "The dune-foamgrid module is an implementation of the dune-grid interface that implements one- and two-dimensional grids in a physical space of arbitrary dimension. The grids are not expected to have a manifold structure, i.e., more than two elements can share a common facet."
git = "https://gitlab.dune-project.org/extensions/dune-foamgrid.git"
title = "dune-foamgrid"
maintainers = "[The dune-foamgrid team](/modules/dune-foamgrid#Maintainers)"
+++

#### dune-foamgrid
<i style="color:blue">dune-foamgrid</i> is an implementation of the <i style="color:blue">dune-grid</i> interface that implements one- and two-dimensional grids in a physical space of arbitrary dimension. The grids are not expected to have a manifold structure, i.e., more than two elements can share a common facet. This makes <i style="color:blue">FoamGrid</i> the grid data structure of choice for simulating structures such as foams, discrete fracture networks, or network flow problems:

<table>
<tr>
<td><img src="/img/root-network-grid-2.png" alt="" width="300" height="244"></td>
<td><img src="/img/fracture-network-grid.png" alt="" width="300" height="244"></td>
</tr>
<tr>
<td colspan=2>One- and two-dimensional network grids in a three-dimensional world.</td>
</tr>
</table>

#### Element Parametrizations
_dune-foamgrid_ allows to use element parametrizations to improve the geometry approximation for curved domains. Each coarse grid element can be given a parametrization that describes an embedding into physical space. This does not influence the grid itself -- elements of a _FoamGrid_ are always affine. However, when refining the grid, the new vertices are determined according to the parametrization. That way, the grid approaches the shape described by the parametrization functions more and more as it gets refined:

<table>
<tr>
<td><img src="/img/parametrized-refinement-0.png" alt="" width="200" height="144"></td>
<td><img src="/img/parametrized-refinement-1.png" alt="" width="200" height="144"></td>
<td><img src="/img/parametrized-refinement-2.png" alt="" width="200" height="144"></td>
</tr>
<tr>
<td><img src="/img/parametrized-refinement-3.png" alt="" width="200" height="144"></td>
<td><img src="/img/parametrized-refinement-4.png" alt="" width="200" height="144"></td>
<td><img src="/img/parametrized-refinement-5.png" alt="" width="200" height="144"></td>
</tr>
<tr>
<td colspan=3>Grid refinement with element parametrizations.</td>
</tr>
</table>

#### Grid Growth
As a unique feature of a dune-grid implementation, a FoamGrid is allowed to grow and shrink, i.e., elements can be added to or removed from the grid at runtime. Data attached to the grid is not invalidated by this. This allows to simulate problems with growing and shrinking domains, which is useful for various network growth and remodeling problems:

<figure>
  <img src="/img/growth_color_new.png" alt="" width="700" height="500">
  <figcaption>Growth of a root system, shown in a lateral (top) and an axial (bottom) view.</figcaption>
</figure>


#### Download
_dune-foamgrid_ is hosted at the [Dune gitlab instance](https://gitlab.dune-project.org/extensions/dune-foamgrid). You can download the current development version using anonymous git:

`git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git`

#### Publications
The concepts of dune-foamgrid are presented in the following publication:

* O. Sander, T. Koch, N. Schröder, B. Flemisch [The Dune FoamGrid implementation for surface and network grids.](https://journals.ub.uni-heidelberg.de/index.php/ans/article/view/28490)
   Archive of Numerical Software, [S.l.], v. 5, n. 1, p. 217-244, apr. 2017. ISSN 2197-8263. doi: 10.11588/ans.2017.1.28490.

#### Maintainers
_dune-foamgrid_ has been written by [Oliver Sander](http://www.math.tu-dresden.de/~osander/), [Carsten Gräser](http://page.mi.fu-berlin.de/graeser/), and [Timo Koch](http://www.hydrosys.uni-stuttgart.de/institut/mitarbeiter/person.php?name=1729), with contributions from [Markus Blatt](http://www.dr-blatt.de/), [Timo Betcke](https://www.sites.google.com/site/timobetcke/), Eugen Salzmann and Wojciech Smigaj.

#### Interface modifications for network grids

Changes to the _dune-grid_ interface are being discussed to better accommodate network grids.
The proposed changes are described in a DICR (Dune Interface Change Request)
[document](dicr-intersections-for-network-grids.pdf).  The TeX sources for this document
are available from the [wiki](http://users.dune-project.org/projects/redesign-of-dune-grid-intersections).
