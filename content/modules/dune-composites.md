+++
# The name of the module.
module = "dune-composites"

# Groups that this module belongs to, please specify (otherwise your module will not
# be reachable from the menu through the groups).
# Currently recognized groups: "core", "disc", "grid", "external", "extension", "user"
group = ["user"]

# List of modules that this module requires
requires = ["dune-common", "dune-geometry", "dune-grid", "dune-localfunctions", "dune-functions", "dune-istl", "dune-pdelab", "dune-typetree"]

# A string with maintainers to be shown in short description, if present.
maintainers = "[The composites team](/modules/dune-composites#Team)"
title = "Dune Composites"

# Main Git repository, uncomment if present
git = "https://gitlab.dune-project.org/anne.reinarz/dune-composites"

# Short description (like one sentence or two). For a more detailed description,
# just write as much as you want in markdown below, uncomment if present.
short = "dune-composites is a [Dune PDELab](/modules/dune-pdelab)  module which implements 3D anisotropic linear elasticity equations in parallel."

# Doxygen documentation: Please specify the following keys to automatically build
# a doxygen documentation for this module. Note, that specifying the git key is
# necessary in this case. All of the keys should be lists of the same length.
# Each entry of the list specifies the parameter for a separate piece of documentation.
# You can use this feature to generate documentation for several branches.
#
# Specify the url, where to build the doxygen documentation
doxygen_url = ["/doxygen/dune-composites/release-2.5"]
# Specify the branch from which to build, omit to build from master
doxygen_branch = ["releases/2.5"]
# Specify to build a a joint documentation from the following list of modules,
# omit, to build a doxygen documentation only for this module. This list will
# be used for all documentations, no list of lists necessary...
#doxygen_modules = []
# Please specify the name of the doxygen documentation, that will be shown on the main page.
doxygen_name = ["dune-composites"]
doxygen_version = ["2.5.1"]
+++

dune-composites is an open-source software framework designed to support the development of high-performance
scalable solvers for partial differential equations for composite applications.
The package provides a single codebase with the following key
features:

* Interfaces to handle composite applications which includes: stacking sequences, complex part geometries, defects and non-standard boundary conditions such as multi-point constraints, or periodicity
conditions.

* To overcome shear locking of standard fintie element both a new 20-node 3D serendipity element (with
full integration) within dune-pdelab, and mesh stabilisation strategies to support reduced integration
element, have been implemented.

* Implementation and interface to a novel, robust preconditioner called GenEO for parallel
Krylov solvers, which exhibits excellent scalability.

* Interface to other state-of-the-art parallel solvers (preconditioners) e.g. Algebraic Multigrid (AMG).

* A code structure which supports both engineering end-users, and those requiring the flexibility to
extend any aspect of the code in a modular way to introduce new applications, solvers or material
models.

### Class Documentation

Class documentation for the module can be found at:

https://www.dune-project.org/doxygen/dune-composites/release-2.5/index.html



### <a name="Team"></a> Team
* [Tim Dodwell](http://emps.exeter.ac.uk/engineering/staff/td336)
* [Anne Reinarz](https://www5.in.tum.de/wiki/index.php/Dr._Anne_Reinarz)
