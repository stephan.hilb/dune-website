+++
module = "dune-prismgrid"
group = ["grid"]
requires = ["dune-grid"]
suggests = ["dune-python"]
maintainers = "Christoph Gersbacher"
git = "https://gitlab.dune-project.org/christoph.gersbacher/dune-prismgrid"
short = "PrismGrid is a grid wrapper for a DUNE grid implementation, adding an additional dimension to the grid using generic prismatic elementslel implementation of the DUNE grid interface."
+++

Dune-PrismGrid
==============

PrismGrid is a grid wrapper for a DUNE grid implementation, adding an additional dimension to the grid using generic prismatic elementslel implementation of the DUNE grid interface.
