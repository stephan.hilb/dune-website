+++
module = "dune-metagrid"
group = ["grid"]
requires = ["dune-grid"]
suggests = ["dune-python"]
maintainers = "Robert Klöfkorn"
git = "https://gitlab.dune-project.org/extensions/dune-metagrid"
short = "A module implementing a variety of DUNE meta grids."
+++

Dune-MetaGrid
================

The DUNE module dune-metagrid provides implementations of the DUNE meta grids:
- CacheItGrid
- CartesianGrid
- FilteredGrid
- IdGrid
- MultiSPGrid
- ParallelGrid
- SphereGrid
