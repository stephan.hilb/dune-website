+++
title = "People"
[menu.main]
parent = "community"
weight = 1
+++

Here is an (incomplete) list of people who are actively contributing to Dune, or who have actively contributed in the past.

If you have questions about Dune, please send them to the [mailing list](/community/mailinglists), unless you have reason to keep your mail private.

* [Peter Bastian][1a] ([IWR, Universität Heidelberg][1b])
* [Markus Blatt][2a] ([Dr. Markus Blatt - HPC-Simulation-Software & Services, Heidelberg][2b])
* [Ansgar Burchardt][3a] ([Institut für Numerische Mathematik, Technische Universität Dresden][3b])
* [Andreas Dedner][4a] ([Warwick Mathematics Institute, University of Warwick, UK][4b])
* [Christian Engwer][5a] ([Institut für Numerische und Angewandte Mathematik, Universität Münster][5b])
* [Jorrit Fahlke (Jö)][6a] ([Institut für Numerische und Angewandte Mathematik, Universität Münster][6b])
* [Christoph Gersbacher][7a] ([Abteilung für Angewandte Mathematik, Universität Freiburg][7b])
* [Carsten Gräser][8a] ([Institut für Mathematik, Freie Universität Berlin][8b])
* [Christoph Grüninger][9a] ([Institut für Wasser- und Umweltsystemmodellierung, Universität Stuttgart][9b])
* [Dominic Kempf][10a] ([IWR, Universität Heidelberg][10b])
* [Robert Klöfkorn][11a] ([International Research Institute of Stavanger, Norway][11b])
* [Steffen Müthing][12a] ([IWR, Universität Heidelberg][12b])
* [Martin Nolte][13a] ([Abteilung für Angewandte Mathematik, Universität Freiburg][13b])
* [Mario Ohlberger][14a] ([Institut für Numerische und Angewandte Mathematik, Universität Münster][14b])
* [Oliver Sander][15a] ([TU Dresden][15b])


[1a]: http://conan.iwr.uni-heidelberg.de/people/peter/
[1b]: http://conan.iwr.uni-heidelberg.de/
[2a]: http://www.dr-blatt.de/
[2b]: http://www.dr-blatt.de/
[3a]: https://www.math.tu-dresden.de/~ansgar/
[3b]: https://tu-dresden.de/mn/math/numerik
[4a]: http://www2.warwick.ac.uk/fac/sci/maths/people/staff/andreas_dedner/
[4b]: http://www2.warwick.ac.uk/fac/sci/maths/
[5a]: http://wwwmath.uni-muenster.de/u/christian.engwer/
[5b]: http://wwwmath1.uni-muenster.de/num/
[6a]: http://wwwmath.uni-muenster.de/num/Arbeitsgruppen/ag_engwer/organization/fahlke/
[6b]: http://wwwmath1.uni-muenster.de/num/
[7a]: http://aam.uni-freiburg.de/abtlg/wissmit/agkr/gersbacher/
[7b]: http://www.mathematik.uni-freiburg.de/IAM/
[8a]: http://page.mi.fu-berlin.de/graeser/
[8b]: http://www.mi.fu-berlin.de/en/math/groups/ag-numerik/index.html
[9a]: http://www.hydrosys.uni-stuttgart.de/institut/mitarbeiter/person.php?name=1450
[9b]: http://www.iws.uni-stuttgart.de/
[10a]: http://conan.iwr.uni-heidelberg.de/people/dominic/
[10b]: http://conan.iwr.uni-heidelberg.de/
[11a]: http://www.iris.no/
[11b]: http://www.iris.no/
[12a]: http://conan.iwr.uni-heidelberg.de/
[12b]: http://conan.iwr.uni-heidelberg.de/
[13a]: https://aam.uni-freiburg.de/mitarb/nolte/
[13b]: https://aam.uni-freiburg.de/
[14a]: http://wwwmath1.uni-muenster.de/u/ohlberger/
[14b]: http://wwwmath1.uni-muenster.de/num/
[15a]: https://www.math.tu-dresden.de/~osander/
[15b]: https://www.math.tu-dresden.de/~osander/
