+++
title = "March 2017 PDELab Meeting Minutes"
+++

Participants: Andreas Nüssing, Christian Engwer, Jorrit Fahlke, Oliver Sander,
Steffen Müthing

Later Carsten also discussed a few things via phone

# Decision Process

- There is a lack of feedback, leading to frustration for contributers and
  less contributions
- There a conflicting requirements: Lectures need a stable base for their
  codes, but this can block new developments
  - Do we want a "PDELabNT"? No, that would mean less testing for older
    functionality.
- Do we want to require "PDELab extension proposals" for bigger changes,
  i.e. small writeups explaning what is to be done?  This may be a good thing
  sometimes, but ultimately we trust the proposer to choose an appropriate
  format for the proposal (i.e. extension proposal, issue, or possibly even
  another repository)

# Contributions

- Every developer should take resposibility: we need to give more feedback, in
  particular on merge requests
- Do we want to organize sprints?  We need to find a date early, three months
  before is probably too late.  Mid of September appear to be a promising date
  among the people present.
- Do we want to do more virtual bug-squashing weeks?  If so
  - we should do occasional video-conferences during the week.  We used
    [Jitsi](https://jitsi.org) during the Dune Developermeeting, which is at
    least semi-open.  There were some problems with the audio though, maybe a
    microphone with integrated noise cancellation will help.  A dedicated
    camera would also be helpful for such events.
  - we should have a chat system where everyone is present during the week
  - The list of bugs to work on should be present before the event.

# Road-Map PDELab

This is stuff that can move to PDELab proper soonish, and does not require any
dune-functions-related facilities.

- [Jö] Adept local operator wrappers: `jacobian_*()` from `alpha_*()` via
  automatic differentiation.
  [#87](https://gitlab.dune-project.org/pdelab/dune-pdelab/issues/87)
- [Marian] Marian has some code for matrix-free solvers for nonlinear
  problems.  Documentation is currently lacking though and needs writing.
  [#88](https://gitlab.dune-project.org/pdelab/dune-pdelab/issues/88)
- [Steffen] Entity sets (in the sense "set of entities of multiple
  codimensions plus `IndexSet`") can be moved to core Dune as special
  `GridView`s.
  [#89](https://gitlab.dune-project.org/pdelab/dune-pdelab/issues/89)
- [Steffen] Entity sets can be removed from PDELab as soon as they are in core
  dune.  They need to stay as a concept though(?).  There is built-in
  `MCMGMapper`-functionality in the `IndexSet` which needs to be removed.
  [#90](https://gitlab.dune-project.org/pdelab/dune-pdelab/issues/90)
- [Steffen] Deprecate `BoundaryGridFunction`
  [#91](https://gitlab.dune-project.org/pdelab/dune-pdelab/issues/91)

# Requirements for a new (dune-functions-inspired) Interface

This is stuff that makes sense only in connection with dune-functions

- [Andreas] `GridOperator` on subsets?
- [Marian] Alternative onestep grid-operator, which passes the weights of the
  spatial and the temporal parts to the single local operator so functions
  need to be evaluated only once.  There is code in EXADUNE, but that needs to
  be extended before general consumption.  See
  [#92](https://gitlab.dune-project.org/pdelab/dune-pdelab/issues/92).
  Possibly relates:
  [#39](https://gitlab.dune-project.org/pdelab/dune-pdelab/issues/39)
- [Steffen] Kill `LocalAssemblerCallSwitch`.
  [#93](https://gitlab.dune-project.org/pdelab/dune-pdelab/issues/93)
- Assembler
  - [Christi] Cut-Cell stuff is too special, the Cut-Cell module should do its
    own assembling.  Probably by hooking into `quadratureRule()`.
  - [Christi] What is needed for GridGlue, in particular entities with
    differing C++ types?
  - [Steffen] What about MultiDomain? Spaces should no longer be restricted
    to subdomains, only GridFunctionSpaces.
- [Steffen] Clean up `gridfunctionspaceutilities.hh`.
  [#94](https://gitlab.dune-project.org/pdelab/dune-pdelab/issues/94).

# List of Current Problems or Works-in-Progress

- The interface of local operators needs revamping
  [#42](https://gitlab.dune-project.org/pdelab/dune-pdelab/issues/42)
- Documentation, in particular
  - Constraints assembler
  - <a name="cc-as-matrix"></a>`ConstraintsContainer` should be considered a
    (very sparse) matrix.  It should be documented as such.  See also
    [cc-interface](#cc-interface).
  - There is a lack of conceptual documentation for developers, decribing
    systems of interacting classes.  In particular: the orderings (see also
    [#71](https://gitlab.dune-project.org/pdelab/dune-pdelab/issues/71),
    [#55](https://gitlab.dune-project.org/pdelab/dune-pdelab/issues/55)).
- There is some code duplication for the sole purpose of providing Doxygen
  documentation
- The `intorder` parameter in `adapt()`
- Defaulting to `NumericalJacobian`, see
  [#45](https://gitlab.dune-project.org/pdelab/dune-pdelab/issues/45).
  This should not be tolerated in the future, since it leads to surprising
  behaviour.
  a) PDELab-provided local operators that use `NumericalJacobian` are
     considered buggy, see
     [#85](https://gitlab.dune-project.org/pdelab/dune-pdelab/issues/85)
  b) `NumericalJacobian` should no longer be advertised, e.g. in courses
  c) Users that still want to use a numerical jacobian should wrap their local
     operators with `add_numerical_jacobian(lop)` or similar, see
     [#86](https://gitlab.dune-project.org/pdelab/dune-pdelab/issues/86)
  d) Provide similar wrapper constructors for automatic differentiation.
- <a name="cc-interface"></a>The constraints containers need an interface for
  external consumption.  This would enhance discovarability and can aid in
  debugging.  It would also allow to use the information they contain in ways
  not forseen inside PDELab. See also [cc-as-matrix](#cc-as-matrix).
- Constraints need to improve composability: sometimes you need the
  unconstrained matrix in addition to the contrained one, e.g. for error
  estimators.  A constrained function space should be considered a different
  function space, just with the same base.
- The ISTL-backends need to clean up their namespacing.  The solverbackends
  are going to be revamped completely, which is why they have not moved to the
  same namespace as the vector/matrix backends.  Oliver Sander is considering
  doing a merge request that aliases the current solver backends into the ISTL
  backend namespace for consistency of description in his book.

# What parts of PDELab is Oliver going to use/touch in his Book

(Obviously, this is the current planning, so things might change)

- dune-functions shim
- local operators
- Dirichlet boundary conditions, but not general contraints
- backends
- adaptivity
- an outlook on how to deal with systems

# Notes

- [Andi Nüssing] The group in Münster (Engwer) has some documentation on
  orderings in its internal Wiki  
  *Note:* this later turned out to be pretty useless.  Maybe Sebastian
   Westerheide has some more usefull documentation somewhere, we'll have to
   ask him.
- [Christi] Do we want to support other kinds of interpolation/projection in
  dune-localfunctions?
- [-> dune-functions] IntersectionSets?

# Phonecall with Carsten
## On `BoundaryGridFunction`
- fufem may have something like a "trace of a base"
- fufem has "boundary patches", which mark a part of a boundary.  They are
  sets of bounary intersections an can be iterated over.
- fufem uses seperate functions for extracting Dirichlet and Neuman boundary
  condition values
- fufem does not have something like a `BoundaryGridFunction`, it would use a
  volume function instead
- dune-function's `GridFunction` would probably work just fine when using an
  `IntersectionSet` in place of an `EntitySet`.  Analytic `GridFunctions`
  though are probably using a `GridView`, they would need to be adapted.
- A `DiscreteGridFunction` for dune-functions and intersections is difficult
  at the moment, because intersections don't have indices in dune-functions.

## Using dune-functions' Discrete Functions in PDELab
Do we need/how to do `.setTime()` in dune-functions?

- Consensus: we do not want `.setTime(...)` syntax in dune-functions
- Proposal by Carsten: extend the interface from `Range(Domain)` to
  `Range(Domain, Args...)`, plus bind/currying.  Think "parameterised
  function".  Provide only differentiation with regards to `Domain`, not
  `Args...`
- Proposal by Christian: Require that derivatives are only obtained from unary
  functions.  If you have an n-ary function you need to bind/curry the other
  arguments first
- Proposal by Carsten: Special dune-functions `bind()`, which allows to bind
  arguments after `Domain`
- Consensus: Keeping an n-ary interface also in the lower levels is easiest.
